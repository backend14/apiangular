﻿using System;
using System.Collections.Generic;

namespace TestWithAngular.Context
{
    public partial class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int Age { get; set; }
        public DateTime Birth { get; set; }
    }
}
