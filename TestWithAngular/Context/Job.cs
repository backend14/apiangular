﻿using System;
using System.Collections.Generic;

namespace TestWithAngular.Context
{
    public partial class Job
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }
    }
}
