﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Pkcs;
using System.Threading.Tasks;
using TestWithAngular.Context;

namespace TestWithAngular.Services
{
    public class UserServices
    {
        private readonly CompanyContext _companyContext;

        public UserServices(CompanyContext companyContext)
        {
            _companyContext = companyContext;
        }

        public async Task<bool> Login(string emailOrUser, string password)
        {
            var loggedUser = await _companyContext.Users.FirstOrDefaultAsync(p => (p.Email == emailOrUser || p.UserName == emailOrUser) && p.Password == password);

            if (loggedUser != null)
                return true;

            return false;
        }

        public async Task<Users> CreateUser(Users user)
        {
            await _companyContext.Users.AddAsync(user);
            await _companyContext.SaveChangesAsync();

            return user;
        }

    }
}
