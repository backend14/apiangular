﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestWithAngular.Context;

namespace TestWithAngular.Services
{
    public class PersonServices
    {
        private readonly CompanyContext _companyContext;

        public PersonServices(CompanyContext companyContext)
        {
            _companyContext = companyContext;
        }

        public async Task<Person> CreatePerson(Person person)
        {
            try
            {
                await _companyContext.Person.AddAsync(person);
                await _companyContext.SaveChangesAsync();

                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Person> UpdatePerson(Person person)
        {
            try
            {
                _companyContext.Person.Update(person);
                await _companyContext.SaveChangesAsync();

                return person;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public async Task<Person> GetPerson(int personId)
        {
            return await _companyContext.Person.FirstOrDefaultAsync(p => p.Id == personId);
        }

        public async Task<List<Person>> ListPersons()
        {
            return await _companyContext.Person.ToListAsync();
        }
    }
}
